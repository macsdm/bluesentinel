from flask import Flask

app = Flask(__name__)


from BlueSentinel import controllers
from BlueSentinel.database import db_session, init_db

init_db()


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()