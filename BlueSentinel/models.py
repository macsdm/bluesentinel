from sqlalchemy import Table, Column, Integer, ForeignKey, String, DateTime, Text, Float, TIMESTAMP
from sqlalchemy.orm import relationship, backref
from BlueSentinel.database import Base


class Room(Base):
    __tablename__ = 'rooms'
    id = Column(Integer, primary_key=True)
    name = Column(String(80))
    regions = relationship('Region', backref='rooms', lazy='dynamic')

    def __init__(self, name=None):
        self.name = name


class Beacon(Base):
    __tablename__ = 'beacons'
    id = Column(Integer, primary_key=True)
    name = Column(String(80))
    regions = relationship('Region', backref='beacons', lazy='dynamic')


class Region(Base):
    __tablename__ = 'regions'
    id = Column(Integer, primary_key=True)
    uuid = Column(Text)
    major = Column(Integer)
    minor = Column(Integer)
    room_id = Column(Integer, ForeignKey('rooms.id'))
    beacon_id = Column(Integer, ForeignKey('beacons.id'))
    samples = relationship('Sample', backref='region', lazy='dynamic')

    def __init__(self, uuid=None, major=None, minor=None):
        self.uuid = uuid
        self.major = major
        self.minor = minor


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    username = Column(Text, unique=True)
    password = Column(String(255))
    samples = relationship('Sample', backref='user', lazy='dynamic')

    def __init__(self, username):
        self.username = username


class Sample(Base):
    __tablename__ = 'samples'
    id = Column(Integer, primary_key=True)
    time_stamp = Column(Text)
    rssi = Column(Integer)
    region_id = Column(Integer, ForeignKey('regions.id'))
    user_id = Column(Integer, ForeignKey('users.id'))

    def __init__(self, time_stamp=None, rssi=None):
        self.time_stamp = time_stamp
        self.rssi = rssi

