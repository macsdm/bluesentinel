from BlueSentinel.database import db_session
from BlueSentinel.models import *
from BlueSentinel import app
from flask import request, redirect, url_for, render_template, Response
from sqlalchemy import desc
import json





@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')


@app.route('/samples/add', methods=['POST'])
def new_sample():
    data = json.loads(request.data.decode('utf-8'))
    samples = data['samples']
    for item in samples:
        region = db_session.query(Region).filter_by(uuid=item['uuid'], major=item['major'], minor=item['minor']).first()
        user = db_session.query(User).filter_by(username=item['username']).first()
        sample = Sample(item['timestamp'], item['rssi'])
        sample.user_id = user.id
        region.samples.append(sample)
        db_session.add(sample)
        db_session.commit()
    return 'Add Json done'


@app.route('/rooms')
def rooms_index():
    rooms = Room.query.all()
    return render_template('rooms.html', rooms=rooms)


@app.route('/rooms/<int:room_id>')
def show_room(room_id):
    r = db_session.query(Room).get(room_id)
    return render_template('room.html', room=r)


@app.route('/rooms/delete/<int:room_id>')
def delete_room(room_id):
    r = db_session.query(Room).get(room_id)
    db_session.delete(r)
    db_session.commit()
    return redirect(url_for('rooms_index'))


@app.route('/rooms/add', methods=['POST', 'GET'])
def add_room():
    if request.method == 'POST':
        r = Room(request.form['room-name'])
        db_session.add(r)
        db_session.commit()
        return redirect(url_for('rooms_index'))
    return render_template('new_room.html')


@app.route('/rooms/<int:room_id>/regions/add', methods=['GET', 'POST'])
def add_region(room_id):
    r = db_session.query(Room).get(room_id)
    if request.method == 'POST':
        region = Region(request.form['region-uuid'], request.form['region-major'], request.form['region-minor'])
        r.regions.append(region)
        db_session.commit()
        return redirect(url_for('show_room', room_id=r.id))
    return render_template('new_region.html', room=r)

@app.route('/rooms/<int:room_id>/regions/delete/<int:region_id>')
def delete_region(room_id,region_id):
    room = db_session.query(Room).get(room_id)
    region = db_session.query(Region).get(region_id)
    db_session.delete(region)
    db_session.commit()
    return redirect(url_for('show_room', room_id=room.id))

@app.route('/regions/fetch', methods=['GET'])
def fetch_regions():
    content = list()
    regions = db_session.query(Region).all()
    for region in regions:
        info = {'uuid': region.uuid, 'major': region.major, 'minor': region.minor}
        content.append(info)
    return Response(json.dumps(content),  mimetype='application/json')

@app.route('/samples')
def show_samples():
    samples = db_session.query(Sample).order_by(desc(Sample.id))
    return render_template('show_samples.html', samples=samples)

@app.route('/users')
def users_index():
    users = User.query.all()
    return render_template('users.html', users=users)

@app.route('/users/add', methods=['POST', 'GET'])
def add_user():
    if request.method == 'POST':
        u = User(request.form['username'])
        db_session.add(u)
        db_session.commit()
        return redirect(url_for('users_index'))
    return render_template('new_user.html')

@app.route('/users/delete/<int:user_id>')
def delete_user(user_id):
    u = db_session.query(User).get(user_id)
    db_session.delete(u)
    db_session.commit()
    return redirect(url_for('users_index'))

#@app.route('/users/<int:user_id>')
#def show_user(user_id):























